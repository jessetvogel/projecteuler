#include <iostream>

int main() {    
    int primes[150000];
    
    unsigned long long sum = 0;
    
    int n = 1;
    int t = 0;
    
    while(n < 2000000) {
        n += 1;
        bool isPrime = true;
        for(int k = 0;k < t; ++k) {
            if(n % primes[k] == 0) {
                isPrime = false;
                break;
            }
        }
        if(isPrime) {
            primes[t++] = n;
            sum += n;
        }
    }
    
    std::cout << sum << std::endl;
    
    return 0;
}
